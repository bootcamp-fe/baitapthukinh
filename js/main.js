import { dataGlasses } from "../data/data.js";

function renderGlassList(){
    let glassListContainer = document.getElementById("vglassesList");
    let contentHTML = "";
    dataGlasses.forEach((item)=>{
        let blockContent =/*HTML*/ `
        <div class="col-4 glass-item">
        <img data-id=${item.id} src=".${item.src}" alt="">
        </div>
        `;
        contentHTML += blockContent;
    })
    glassListContainer.innerHTML = contentHTML;
}

renderGlassList();

let glassList = document.querySelectorAll(".glass-item img");
glassList.forEach((item,index)=>{
    item.onclick = function(){
        tryThisGlass(index);
    };
});

function tryThisGlass(index){
    let glassData = dataGlasses[index];
    // Render Glass
    document.getElementById("avatar").innerHTML = `<img src="${glassData.virtualImg}" alt="${glassData.name}">`;
    // Render Glass Info
    let infoHTML =/*HTML*/`
    <h4>${glassData.name} - ${glassData.brand} (${glassData.color})</h4>
    <div class="btn btn-danger btn-sm">$${glassData.price}</div>
    <span class="text-success">Stocking</span>
    <p class="mt-2">${glassData.description}</p>
    `;
    document.getElementById("glassesInfo").style.display = "block";
    document.getElementById("glassesInfo").innerHTML = infoHTML;
}

function removeGlasses(remove){
    let wearingGlass = document.querySelector("#avatar img");
    if(wearingGlass!= null && !remove){
    document.querySelector("#avatar img").style.display = "none";
    } else if (wearingGlass!= null) {
    document.querySelector("#avatar img").style.display = "block";
    }
}

window.removeGlasses = removeGlasses;